<?php

namespace Drupal\field_label_long_text\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'field_label_long_text.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_label_long_text_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Label Type'),
      '#options' => [
        'textfield' => $this->t('Textfield'),
        'textarea' => $this->t('Text area (multiple rows)'),
      ],
      '#default_value' => $config->get('type'),
      '#description' => $this->t('Choose a type of label in the manage field settings.'),
    ];

    $form['maxlength'] = [
      '#type' => 'number',
      '#title' => $this->t('Maxlength'),
      '#default_value' => $config->get('maxlength') ? $config->get('maxlength') : 128,
      '#states' => [
        'invisible' => [
          ':input[name="type"]' => ['value' => 'textarea'],
        ],
      ],
      '#min' => 128,
      '#description' => $this->t('Set the maximum value characters for label field only when using textfield.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('type', $form_state->getValue('type'))
      ->set('maxlength', $form_state->getValue('maxlength'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
