CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

##INTRODUCTION
------------

 This is a very simple and light module that helps to extend for character 
 limit for field labels.
 by default, Drupal provides 128 characters. but using this module can 
 increase the text limit to any length by setting up the configuration 
 form setting page.

 This module also provides the ability to change the type of the label field 
 from text field to text area where you are allowed to put the maximum 
 text as possible to explain to the users about your field.

##REQUIREMENTS
------------

This module does not required any extra contibuted modules.

RECOMMENDED MODULES
-------------------

 * No extra contributed module is required.
 * It just usses the core user module.

##INSTALLATION
------------

 * Install as usual.
 * composer command : composer require 'drupal/field_label_long_text

##CONFIGURATION
-------------

 * Go to the /admin/config/field_lable_long_text 
     and configure the type of field. If you are using text field 
     then you have a choice to increase the limit of the character length.

##MAINTAINERS
-----------

Current maintainers:

 * Anok-Rathod (https://www.drupal.org/user/3213311)
